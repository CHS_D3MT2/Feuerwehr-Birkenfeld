<?php 
/* Template Name: Inhaltselemente */ 
get_header(); 
?>

	<main class="site-main" role="main">
		<?php

			if(have_rows('content_elements')):
				while(have_rows('content_elements')): the_row();
					get_template_part('templates/template-parts/template-hero');
				endwhile;
			endif;

		?>
		<?php edit_post_link(); ?>
	</main>

<?php 
get_footer(); 
?>
