<?php 
/* Template Name: Homepage */ 
get_header(); 
?>

	<main class="site-main" role="main">
		<?php
			if(have_rows('content_elements')):
				while(have_rows('content_elements')): the_row();
					get_template_part('templates/template-parts/template-hero');
				endwhile;
			endif;
		?>
		<div class="news-dates">
			<div class="wrapper">
				<?php get_template_part('templates/template-parts/template-loop'); ?>
				<?php get_template_part('templates/template-parts/template-dates'); ?>
			</div>
		</div>

		<?php // get_template_part('templates/template-parts/template-about'); ?>
	</main>

<?php get_footer(); ?>
