<?php 

// Navigation

?>

<header class="site-header">
  <div class="notice notice-js">
		<div class="wrapper">
      <div class="notice-headline">Kein Javascript entdeckt!</div>
      <p class="notice-text">Es scheint als wäre Javascript, welches für die optimale Darstellung dieser Seite vorausgesetzt wird, deaktiviert. Wie sie diese Einstellung umkehren, finden sie im unten aufgeführten Link. Wir danken Ihnen für Ihr Verständnis.</p>
      <a class="button button-cta" href="https://enable-javascript.com/de/" target="_blank">Javascript aktivieren</a>
		</div>
	</div>
  <nav class="meta-navigation">
    <div class="wrapper">
      <?php wp_nav_menu(array('theme_location' => 'meta-navigation')); ?>
    </div>
  </nav>
  <nav class="main-navigation">
    <div class="wrapper">
      <div class="logo">
        <a href="<?= get_home_url(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-fw-birkenfeld.png" alt="<?php bloginfo('name'); ?>">
        </a>
      </div>
      <?php wp_nav_menu(array('theme_location' => 'main-navigation')); ?>
    </div>
  </nav>
</header>