<?php

// Hero Container

if(get_row_layout() == 'hero_container'):

  $hero_container_headline = get_sub_field('hero_container_headline');
  $hero_container_subline = get_sub_field('hero_container_subline');
  $hero_container_cta = get_sub_field('hero_container_cta');
  $hero_container_slider = get_sub_field('hero_container_slider');
  $hero_container_image = get_sub_field('hero_container_image');
  $hero_container_slides = get_sub_field('hero_container_slides');

  echo "
    <div class='hero-container'>
      <div class='wrapper'>
        <div class='hero-container-content'>
          <div class='hero-container-inner-content'><small>";
          bloginfo('name');
          echo "</small>
            <div class='hero-container-headline'>$hero_container_headline</div>
            <div class='hero-container-subline'>$hero_container_subline</div>
            <a class='button button-cta' href='$hero_container_cta[url]' target='$hero_container_cta[target]'>$hero_container_cta[title]</a>
          </div>
        </div>";
      
      if( $hero_container_slider == 1 ) {
        echo "
          <div class='hero-container-slider swiper-container'>
            <div class='swiper-wrapper'>";
        
          if(have_rows('hero_container_slides')):
            while(have_rows('hero_container_slides')): the_row();
              $slide = get_sub_field('hero_container_slides');
              echo $slide;
            endwhile;
          endif;

        echo "
            </div><!-- swiper-wrapper -->
          </div><!-- hero-container-slider -->
        ";
      } else {
        echo "</div><!-- wrapper -->
              <figure class='background-image'>
                <img src='$hero_container_image' />
              </figure>";
      }

  echo "</div><!-- hero-container -->";
endif;