<?php

// The actual way...

if(have_rows('about_us')):
  ?>
  <div class="about-us-container">
  <?php
  while(have_rows('about_us')):the_row();
    $about_us_headline = get_sub_field('about_us_headline');
    $about_us_subline = get_sub_field('about_us_subline');
    $about_us_counter = get_sub_field('about_us_counter');
    $about_us_text = get_sub_field('about_us_text');
    $about_us_image = get_sub_field('about_us_image');
    ?>

    <?php if($about_us_image): ?>
    <div class="column">
      <figure class="about-us-image">
        <img 
          src="<?= $about_us_image['url'] ?>" 
          alt="<?= $about_us_image['alt'] ?>" 
          title="<?= $about_us_image['title'] ?>"
        />
      </figure>
    </div>
    <?php endif; ?>
    <div class="column">
    <h2 class="about-us-headline"><?= $about_us_headline ?></h2>
    <p class="about-us-subline"><?= $about_us_subline ?></p>
      <?php
        if(have_rows('about_us_counter')):
          ?>
          <div class="about-us-counter">
          <?php
          while(have_rows('about_us_counter')): the_row();
            $integer = get_sub_field('about_us_counter_int');
            $text = get_sub_field('about_us_text');
            ?>
              <div class="about-us-counter-item">
                <span class="number"><?= $integer ?></span>
                <span class="description"><?= $text ?></span>
              </div>
            <?php
          endwhile;
          ?>
          </div>
          <?php
        endif;
        ?>
        <div class="about-us-main-text"><?= $about_us_text ?></div>
        <?php
      ?>
    </div>
    <?php
  endwhile;
  ?>
  </div>
  <?php
endif;