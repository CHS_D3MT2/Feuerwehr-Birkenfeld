<?php 

// Dates widget

?>

<div class="dates">
  <div class="dates-head">
    <h2 class="dates-headline">Termine</h2>
    <a href="#" class="show-all">Alle anzeigen</a>
  </div>
  <ul class="dates-items">
  <?php
    $loop = new WP_Query(array('post_type' => 'date',	'meta_key' => 'date',
    'orderby'	=> 'meta_value_num', 'order' => 'ASC', 'posts_per_page' => 5) );
    if ( $loop->have_posts() ) :
      while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <li class="dates-item">
          <a href="<?php the_permalink(); ?>">
            <span class="date">
              <?php 
                $date = get_field('date', false, false);
                $date = new DateTime($date);
                echo "<strong>" . $date->format('d') . "</strong>";
                echo "<small>" . $date->format('M') . "</small>";
              ?>
            </span>
            <h3 class="title">
              <strong><?php the_title(); ?></strong>
              <small><?php the_field('subtitle'); ?></small>
            </h3>
          </a>
        </li>
      <?php endwhile;
      if (  $loop->max_num_pages > 1 ) : ?>
        <div id="nav-below" class="navigation">
            <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'domain' ) ); ?></div>
            <div class="nav-next"><?php previous_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'domain' ) ); ?></div>
        </div>
      <?php endif;
    endif;
    wp_reset_postdata();
?>
  </ul>
</div>