<?php 

// News Loop

?>

<div class="news-loop">
  <div class="news-loop-head">
    <h2 class="news-loop-headline">Aktuelles</h2>
    <a href="#" class="show-all">Alle anzeigen</a>
    <ul class="slider-controls">
      <button class="slider-controls-prev"><span>Vorherige</span></button>
      <button class="slider-controls-next"><span>Nächste</span></button>
    </ul>
  </div>
  <ul class="news-loop-items">
    <?php
    global $post;
    $args = array('posts_per_page' => 3);

    $myposts = get_posts( $args );
    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

      <li class="news-loop-item">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <figure class="image">
            <?php the_post_thumbnail('front-page-news'); ?>
          </figure>
          <h3 class="title">
            <?php the_title(); ?>
          </h3>
          <p class="excerpt"><?php the_excerpt(80); ?></p>
          <p class="date"><?php echo get_the_date('g. F Y'); ?></p>
        </a>
      </li>
    <?php 
    endforeach; 
    wp_reset_postdata();
    ?>
  </ul>
</div>