
			<footer class="footer" role="footer">
				<div class="wrapper">
					<?php
					if(have_rows('location', 'option')): ?>
						<div class='footer-meta'>
							<div class='footer-meta-columns'>
							<?php
							while(have_rows('location', 'option')):
								the_row(); ?>
								<div class='footer-meta-column'>
								<?php
								$location_name = get_sub_field('location_name');
								$location_address = get_sub_field('location_address');
								$location_phone = get_sub_field('location_phone');
								$location_email = get_sub_field('location_email');
								?>
								<div class='footer-meta-headline'><?= $location_name ?></div>
								<div class='footer-meta-address'><?= $location_address ?></div>
								<div class='footer-meta-phone'><a href='tel:$location_phone'><?= $location_phone ?></a></div>
								<div class='footer-meta-email'><a href='mailto:$location_email'><?= $location_email ?></a></div>

								</div><!-- footer-meta-column -->
							<?php endwhile; ?>
							</div><!-- footer-meta-columns -->

							<div class="footer-meta-navigation">
								<div class="footer-meta-headline">Navigation</div>
								<?php wp_nav_menu(array('theme_location' => 'main-navigation')); ?>
							</div><!-- footer-meta-navigation -->
						</div><!-- footer-meta -->
					<?php endif; ?>

					<div class="footer-copyright">
						<?php wp_nav_menu(array('theme_location' => 'footer-social-links')); ?>
						<p class="footer-copyright-text">&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>.</p>
						<?php wp_nav_menu(array('theme_location' => 'footer-meta-navigation')); ?>
					</div>
				</div>
			</footer>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts.min.js"></script>
		<?php wp_footer(); ?>
	</body>
</html>
