(function(){

  const $window = $(window);

  $window.on('scroll', function(){
    const scrollTop = window.scrollY;
    const $meta = $('.meta-navigation');
    const metaHeight = $meta.innerHeight();
    const $nav = $('.main-navigation');

    $nav.toggleClass('sticky', scrollTop > metaHeight);
  });

})();