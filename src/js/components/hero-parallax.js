(function(){

  const $window = $(window);

  $window.on('scroll', function() {
    const scrollTop = window.scrollY;
    const $bgImage = $('.hero-container .background-image');

    $bgImage.css({
      'transform': 'translateY(' + scrollTop / 5 + 'px)',
    });
  });

})();