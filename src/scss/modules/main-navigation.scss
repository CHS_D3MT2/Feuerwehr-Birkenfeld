
.main-navigation {
  background: $color-background;
  width: 100%;
  transition: box-shadow .3s ease;

  .wrapper {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .logo {
    padding: 2.3rem 0;

    a {
      border: 0;
    }
  }

  .menu {
    display: flex;
    margin-right: -2.5rem;

    a {
      display: block;
      padding: 4.1rem 2.6rem;
      font-size: 1.6rem;
      font-family: $font-stack-headline;
      font-weight: 700;
      text-transform: uppercase;
      color: $color-font-headline;
      border: 0;
      transition: background .3s $easing;
    }

    li {
      &:hover {
        & > a {
          background: darken($color-background, 2);
        }
      }
    }
  }

  .sub-menu {
    display: block;
    pointer-events: none;
    opacity: 0;
    transform: rotateX(-90deg);
    transform-origin: top center;
    position: absolute;
    background: darken($color-background,5);
    top: 100%;
    left: 0;
    width: 100%;
    z-index: 11;
    transition: opacity .3s $easing,
                transform .3s $easing;

    li {
      & + li {
        border-top: 1px solid $color-light-gray;
        border-left: 0;
      }
    }

    a {
      padding: 2.1rem 2.6rem;
      font-size: 1.4rem;
    }
  }

  li {
    display: block;

    & + li {
      border-left: 1px solid $color-light-gray;
    }

    &.menu-item-has-children {
      position: relative;

      & > a {
        @extend %icon;

        &::after {
          content: '\f107';
          margin-left: .5rem;
        }
      }

      &:hover {
        & > a {
          &::after {
            content: '\f106';
          }
        }

        .sub-menu {
          opacity: 1;
          transform: rotateX(0);
          pointer-events: all;
        }
      }
    }
  }

  &.sticky {
    position: fixed;
    z-index: 999;
    top: 0;
    left: 0;
    box-shadow: 0px 10px 36px rgba(0,0,0,.08);
    background: rgba($color-background, .98);

    .logo {
      padding: 0.9rem 0;

      img {
        width: 115px;
      }
    }

    .menu {
      a {
        font-size: 1.4rem;
        padding: 2.6rem 2.6rem;
      }
    }
    
    .sub-menu {
      a {
        font-size: 1.2rem;
        padding: 1.8rem 2.6rem;
      }
    }
  }
}